import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../services/api-service.service';

@Component({
  selector: 'app-my-posts',
  templateUrl: './my-posts.component.html',
  styleUrls: ['./my-posts.component.css']
})
export class MyPostsComponent implements OnInit {
  articles:any
  posts = [];
  localStorage:any
  constructor(
    private api:ApiServiceService
  ) { }

  ngOnInit() {
    this.my_feeds();
    this.my_posts();
    this.localStorage = localStorage;
  }
  my_feeds(){
    this.api.get_feeds().toPromise().then((data:any)=>{
      // console.log(data)
      this.articles = data.articles
    })
  }

  my_posts(){
    this.api.get_articles().toPromise().then((data:any)=>{
      // console.log(data)
      for (let i = 0; i < data.articles.length; i++) {
        const element = data.articles[i];
        if (element.author.username == localStorage.getItem('username')) {
          this.posts.push(element);
        }
      }
    })
  }
  delete(slug){
    this.api.delete_article(slug).toPromise().then((data:any)=>{
      // console.log(data)
      this.my_posts()
      setTimeout(() => {
        window.location.reload();
      }, 300);
    })
  }
}
