import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from 'src/app/services/api-service.service';

@Component({
  selector: 'app-all-tags',
  templateUrl: './all-tags.component.html',
  styleUrls: ['./all-tags.component.css']
})
export class AllTagsComponent implements OnInit {
  all_tags:any;
  constructor(
    private api:ApiServiceService
  ) { }

  ngOnInit() {
    this.tags();
  }

  tags(){
    this.api.get_tags().toPromise().then((data:any)=>{
      // console.log(data)
      this.all_tags = data.tags;
    })
  }

}
