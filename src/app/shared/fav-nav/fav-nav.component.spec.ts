import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavNavComponent } from './fav-nav.component';

describe('FavNavComponent', () => {
  let component: FavNavComponent;
  let fixture: ComponentFixture<FavNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
