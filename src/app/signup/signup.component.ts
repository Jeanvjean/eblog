import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../services/api-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  user = {
    username:"",
    email:"",
    password:""
  }
  constructor(
    private api:ApiServiceService,
    private router:Router
  ) { }

  ngOnInit() {
  }

  signup(){
   var data = {
     "user":{
      username:this.user.username,
      email:this.user.email,
      password:this.user.password
     }
    }

    this.api.create_user(data).toPromise().then((data:any)=>{
      // console.log(data);
      this.router.navigate(['/signin']);
    })
  }

}
