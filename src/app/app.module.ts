import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';

import {NgxPaginationModule} from 'ngx-pagination';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { SignupComponent } from './signup/signup.component';
import { SigninComponent } from './signin/signin.component';
import { SettingdComponent } from './settingd/settingd.component';
import { FooterComponent } from './shared/footer/footer.component';
import { ArticleComponent } from './article/article.component';
import { TagsComponent } from './tags/tags.component';
import { FavouriteComponent } from './favourite/favourite.component';
import { MyPostsComponent } from './my-posts/my-posts.component';
import { CreatePostComponent } from './create-post/create-post.component';
import { YourFeedsComponent } from './your-feeds/your-feeds.component';
import { AllTagsComponent } from './shared/all-tags/all-tags.component';
import { FavNavComponent } from './shared/fav-nav/fav-nav.component';
import { HomeNavComponent } from './shared/home-nav/home-nav.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    SignupComponent,
    SigninComponent,
    SettingdComponent,
    FooterComponent,
    ArticleComponent,
    TagsComponent,
    FavouriteComponent,
    MyPostsComponent,
    CreatePostComponent,
    YourFeedsComponent,
    AllTagsComponent,
    FavNavComponent,
    HomeNavComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    NgxPaginationModule,
    NgMultiSelectDropDownModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
