import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../services/api-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  user = {
    email:"",
    password:""
  }
  constructor(
    private api: ApiServiceService,
    private router:Router
  ) { }

  ngOnInit() {
  }

  signin(){
    var data = {
      "user":{
        email:this.user.email,
        password:this.user.password
      }
    }
    this.api.signin(data).toPromise().then((data:any)=>{
      // console.log(data);
      localStorage.setItem('token',data.user.token);
      localStorage.setItem('username',data.user.username);
      this.router.navigate(['/']);
    })
  }

}
