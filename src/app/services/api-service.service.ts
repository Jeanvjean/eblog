import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BASE_API } from "./api.service";


@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
base_url = BASE_API;

  constructor(private http: HttpClient) { }

  //authentication
  create_user(data){
    return this.http.post(`${this.base_url}users`,data)
  }

  signin(data){
    return this.http.post(`${this.base_url}users/login`,data)
  }

  updateUser(data){
    return this.http.put(`${this.base_url}users`,data,{
      headers: new HttpHeaders({ 'Authorization': localStorage.getItem('token') })
    });
  }

  users(){
    return this.http.get(`${this.base_url}users`)
  }

  //profile

  profile(username){
    return this.http.get(`${this.base_url}profiles/${username}`)
  }

  follow(username){
    return this.http.post(`${this.base_url}profiles/${username}/follow`,{})
  }

  unfollow(username){
    return this.http.delete(`${this.base_url}/profiles/${username}/follow`,{
      headers: new HttpHeaders({ 'Authorization': localStorage.getItem('token') })
    })
  }

  //favourites
  make_fave(slug){
    return this.http.post(`${this.base_url}articles/${slug}/favorite`,{},{
      headers: new HttpHeaders({ 'Authorization': localStorage.getItem('token') })
    })
  }

  delete_fave(slug){
    return this.http.delete(`${this.base_url}articles/${slug}/favorite`)
  }

  //tags
  get_tags(){
    return this.http.get(`${this.base_url}tags`)
  }

  //comments
  post_comment(slug,data){
    return this.http.post(`${this.base_url}articles/${slug}/comments`,data,{
      headers: new HttpHeaders({ 'Authorization': localStorage.getItem('token') })
    })
  }

  remove_comment(id,slug){
    return this.http.delete(`${this.base_url}articles/${slug}/comments/${id}`,{
      headers: new HttpHeaders({ 'Authorization': localStorage.getItem('token') })
    })
  }

  comments(slug){
    return this.http.get(`${this.base_url}articles/${slug}/comments`)
  }

  //articles

  create_article(data){
    return this.http.post(`${this.base_url}articles`,data,{
      headers: new HttpHeaders({ 'Authorization': localStorage.getItem('token') })
    })
  }

  get_article(slug){
    return this.http.get(`${this.base_url}articles/${slug}`)
  }

  delete_article(slug){
    return this.http.delete(`${this.base_url}articles/${slug}`,{
      headers: new HttpHeaders({ 'Authorization': localStorage.getItem('token') })
    })
  }

  get_articles(){
    return this.http.get(`${this.base_url}articles`)
  }

  get_feeds(){
    return this.http.get(`${this.base_url}articles/feed`,{
      headers: new HttpHeaders({ 'Authorization': localStorage.getItem('token') })
    })
  }

  update_article(data,slug){
    return this.http.put(`${this.base_url}articles/${slug}`,data,{
      headers: new HttpHeaders({ 'Authorization': localStorage.getItem('token') })
    })
  }
}
