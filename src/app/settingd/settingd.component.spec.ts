import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingdComponent } from './settingd.component';

describe('SettingdComponent', () => {
  let component: SettingdComponent;
  let fixture: ComponentFixture<SettingdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
