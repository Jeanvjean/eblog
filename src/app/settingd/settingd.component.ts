import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../services/api-service.service';

@Component({
  selector: 'app-settingd',
  templateUrl: './settingd.component.html',
  styleUrls: ['./settingd.component.css']
})
export class SettingdComponent implements OnInit {
  localStorage:any
  profile:any
  constructor(
    private api:ApiServiceService
  ) { }

  ngOnInit() {
    this.localStorage = localStorage;
    this.user();
  }

  logout(){
    localStorage.clear();
    window.location.reload();
  }

  user(){
    var username = this.localStorage.getItem('username');
    this.api.profile(username).toPromise().then((data:any)=>{
      // console.log(data);
      this.profile = data.profile;
    })
  }

  update(){
    var data={
      "profile":{
        image:this.profile.image,
        username:this.profile.username,
        bio:this.profile.bio
      }
    }
    this.api.updateUser(data).toPromise().then((data:any)=>{
      // console.log(data);
      this.user()
    })
  }

}
