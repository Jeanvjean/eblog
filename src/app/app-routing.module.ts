import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { SettingdComponent } from './settingd/settingd.component';
import { ArticleComponent } from './article/article.component';
import { TagsComponent } from './tags/tags.component';
import { FavouriteComponent } from './favourite/favourite.component';
import { MyPostsComponent } from './my-posts/my-posts.component';
import { CreatePostComponent } from './create-post/create-post.component';
import { AuthGuard } from './auth/auth.guard';
import { YourFeedsComponent } from './your-feeds/your-feeds.component';


const routes: Routes = [
  {path:'',component:HomeComponent},

  {path:'signin',component:SigninComponent},
  {path:'signup',component:SignupComponent},
  {path:'settings',component:SettingdComponent,canActivate:[AuthGuard]},
  {path:'article/:slug',component:ArticleComponent},
  {path:'tags/:tag',component:TagsComponent},
  {path:'favorite',component:FavouriteComponent,canActivate:[AuthGuard]},
  {path:'my_posts',component:MyPostsComponent,canActivate:[AuthGuard]},
  {path:'create_article',component:CreatePostComponent,canActivate:[AuthGuard]},
  {path:'global_feeds',component:YourFeedsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
