import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../services/api-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConditionalExpr } from '@angular/compiler';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  article:any

  feat_tags:any
  dropdownSettings:{}
  show = false;
  comment={
    body:""
  }
  acomments:any;
  localStorage:any
  constructor(
    private api: ApiServiceService,
    private route: ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit() {
    this.localStorage = localStorage;
    this.read();
    this.tags();
    this.dropdownSettings = {
      singleSelection: false,
      idField: '_id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }

  hide(){
    if (this.show) {
      this.show = false
    } else {
      this.show = true
    }
  }

  tags(){
    this.api.get_tags().toPromise().then((data:any)=>{
      // console.log(data);
      this.feat_tags = data.tags
    })
  }

  read(){
    var slug = this.route.snapshot.params['slug']
    this.api.get_article(slug).toPromise().then((data:any)=>{
      console.log(data);
      this.article = data.article;
      setTimeout(() => {
        this.article_comments(slug);
      }, 300);
    });
  }

  favorite(slug){
    this.api.make_fave(slug).toPromise().then((data:any)=>{
      // console.log(data)
      this.read();
    })
  }

  update(){
    var slug = this.route.snapshot.params['slug'];
    var data = {
      "article":{
        title:this.article.title,
        description:this.article.description,
        tagList:this.article.tags,
        body:this.article.body
      }
    }
    this.api.update_article(data,slug).toPromise().then((data:any)=>{
      // console.log(data)
      this.read();
      this.hide();
    })
  }

  send_comment(slug){
    var data = {
      "comment":{
        body:this.comment.body
      }
    }
    this.api.post_comment(slug,data).toPromise().then((data:any)=>{
      // console.log(data)
      this.read();
    })
  }

  article_comments(slug){
    // var slug = this.route.snapshot.params['slug']
    this.api.comments(slug).toPromise().then((data:any)=>{
      // console.log(data);
      this.acomments = data.comments;
    })
  }

  delete_comment(id,slug){
    this.api.remove_comment(id,slug).toPromise().then((data)=>{
      // console.log(data)
      this.read()
    })
  }

  follow(username){
    this.api.follow(username).toPromise().then((data:any)=>{
      console.log(data);
      this.read();
    })
  }
  unfollow(username){
    this.api.follow(username).toPromise().then((data:any)=>{
      console.log(data);
      this.read();
    })
  }

}
