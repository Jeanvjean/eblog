import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../services/api-service.service';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.css']
})
export class FavouriteComponent implements OnInit {
  favorite=[];
  constructor(
    private api: ApiServiceService
  ) { }

  ngOnInit() {
    this.fav();
  }

  fav(){
    this.api.get_articles().toPromise().then((data:any)=>{
      // console.log(data)
      for (let i = 0; i < data.articles.length; i++) {
        const element = data.articles[i];
        if (element.favorited) {
          this.favorite.push(element);
        }
      }
      // console.log(this.favorite)
    })
  }

}
