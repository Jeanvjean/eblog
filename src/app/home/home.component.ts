import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../services/api-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  articles:any;
  p:any;
  constructor(
    private api: ApiServiceService
  ) { }

  ngOnInit() {
    this.get_articles();
  }

  get_articles(){
    this.api.get_articles().toPromise().then((data:any)=>{
      // console.log(data);
      this.articles = data.articles;
    })
  }

}
