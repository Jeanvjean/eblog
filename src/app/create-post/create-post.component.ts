import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../services/api-service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {
  feat_tags:any;
  post = {
    title:"",
    about:"",
    body:"",
    tags:""
  }
  dropdownSettings:{}
  constructor(
    private api: ApiServiceService,
    private route: ActivatedRoute,
    private router: Router
  ) { }



  ngOnInit() {
    this.tags();
    this.dropdownSettings = {
      singleSelection: false,
      idField: '_id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }

  // addTag() {
  //       this.post.tags.push({ tag:""});
  //   }
  //   deleteTag(index) {
  //       this.post.tags.splice(index, 1);
  //   }
  //   trackByFn(index: any, item: any) {
  //       return index;
  //   }

  tags(){
    this.api.get_tags().toPromise().then((data:any)=>{
      // console.log(data);
      this.feat_tags = data.tags
    })
  }

  create_post(){
    var data = {
      "article":{
        title:this.post.title,
        description:this.post.about,
        tagList:this.post.tags,
        body:this.post.body
      }
    }

    this.api.create_article(data).toPromise().then((data:any)=>{
      // console.log(data);
      this.router.navigate(['/my_posts']);
    });
  }

}
