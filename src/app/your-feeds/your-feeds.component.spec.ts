import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YourFeedsComponent } from './your-feeds.component';

describe('YourFeedsComponent', () => {
  let component: YourFeedsComponent;
  let fixture: ComponentFixture<YourFeedsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YourFeedsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YourFeedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
