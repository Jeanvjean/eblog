import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../services/api-service.service';

@Component({
  selector: 'app-your-feeds',
  templateUrl: './your-feeds.component.html',
  styleUrls: ['./your-feeds.component.css']
})
export class YourFeedsComponent implements OnInit {
  feeds:any
  constructor(
    private api: ApiServiceService
  ) { }

  ngOnInit() {
    this.global();
  }

  global(){
    this.api.get_feeds().toPromise().then((data:any)=>{
      // console.log(data)
      this.feeds = data.articles;
    })
  }

}
